package models

type Post struct {
	Id      string
	Title   string
	Content string
}

func AddPost(Id, Title, Content string) *Post {
	return (&Post{Id, Title, Content})
}
