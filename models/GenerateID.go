package models

import (
	"crypto/rand"
	"fmt"
)

func GenerateId() string {
	id := make([]byte, 16)
	rand.Read(id)
	return fmt.Sprintf("%x", id)
}
