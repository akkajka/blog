package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"

	"./models"
)

var posts map[string]*models.Post

func IndexPage(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("pages/index.html", "pages/header.html", "pages/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	t.ExecuteTemplate(w, "index", posts)
}

func WritePage(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("pages/write.html", "pages/header.html", "pages/footer.html")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	t.ExecuteTemplate(w, "write", nil)
}

func SavePost(w http.ResponseWriter, r *http.Request) {
	id := models.GenerateId()
	title := r.FormValue("Title")
	content := r.FormValue("Content")

	post := models.AddPost(id, title, content)
	posts[post.Id] = post

	http.Redirect(w, r, "/", 302)
}

func main() {

	port := ":8080"

	posts = make(map[string]*models.Post)

	http.Handle("/pages/", http.StripPrefix("/pages/", http.FileServer(http.Dir("./pages/"))))
	http.Handle("/pages/images/", http.StripPrefix("/pages/images/", http.FileServer(http.Dir("./pages/images"))))
	http.HandleFunc("/", IndexPage)
	http.HandleFunc("/write", WritePage)
	http.HandleFunc("/savepost", SavePost)
	err := http.ListenAndServe(port, nil)
	if err != nil {
		log.Fatal("ListenAndServe")
		os.Exit(-1)
	}
}
